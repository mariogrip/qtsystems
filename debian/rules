#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

NULL =

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

export QT_SELECT := qt5

include /usr/share/dpkg/architecture.mk

CONFIG = tests ofono upower
ifeq ($(DEB_HOST_ARCH_OS),linux)
	CONFIG += udisks
endif

QT_MODULE_VERSION = $(shell cat .qmake.conf | grep MODULE_VERSION | sed -re 's/MODULE_VERSION\s*=\s*([^\s]+)\s*/\1/')

%:
	dh $@ --with pkgkde_symbolshelper

override_dh_auto_configure:
	@echo "# Running syncqt.pl (QT_MODULE_VERSION=$(QT_MODULE_VERSION))"
	cd src/systeminfo/ && perl /usr/lib/qt5/bin/syncqt.pl -module QtSystemInfo -version $(QT_MODULE_VERSION) -outdir $(CURDIR) -builddir $(CURDIR) $(CURDIR) && cd - 1>/dev/null
	cd src/publishsubscribe/ && perl /usr/lib/qt5/bin/syncqt.pl -module QtPublishSubscribe -version $(QT_MODULE_VERSION) -outdir $(CURDIR) -builddir $(CURDIR) $(CURDIR) && cd - 1>/dev/null
	cd src/serviceframework/ && perl /usr/lib/qt5/bin/syncqt.pl -module QtServiceFramework -version $(QT_MODULE_VERSION) -outdir $(CURDIR) -builddir $(CURDIR) $(CURDIR) && cd - 1>/dev/null
	dh_auto_configure -- CONFIG+="$(CONFIG)" QT_BUILD_PARTS+=tests

override_dh_auto_install:
	dh_auto_install --no-parallel

	# Fix wrong path in pkgconfig files
	find $(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig -type f -name '*.pc' \
	-exec sed -i -e 's/$(DEB_HOST_MULTIARCH)\/$(DEB_HOST_MULTIARCH)/$(DEB_HOST_MULTIARCH)/g' {} \;

	# Remove libtool-like files
	rm -f debian/tmp/usr/lib/*/*.la

	# Remove superfluous qmake .pro file
	rm -f debian/tmp/usr/lib/*/qt5/examples/examples.pro

override_dh_missing:
	dh_missing --fail-missing

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	mkdir -p $(CURDIR)/debian/test-home/
	env CURDIR=$(CURDIR) \
	    HOME=$(CURDIR)/debian/test-home \
	    XDG_DATA_HOME=$(CURDIR)/debian/test-home/.local/share \
	    LD_LIBRARY_PATH=$(CURDIR)/lib \
	    dbus-test-runner --keep-env --bus-type=both -m 600 -t debian/run-tests.sh
endif

override_dh_clean:
	rm -Rfv $(CURDIR)/debian/test-home/
	dh_clean

get-orig-source:
	uscan --noconf --force-download --rename --download-current-version --destdir=..
